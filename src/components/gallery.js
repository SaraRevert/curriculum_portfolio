import React from 'react';
import Lightbox from "react-awesome-lightbox";
import "react-awesome-lightbox/build/style.css";


const AllImages = (props) =>{
    let to_return = []
    to_return = props.props.images.map((images,index) =>{
        return [
            <div key={Math.random()} className="photo" onClick={(e)=>{props.props.onOpen(index)}} >
                <img class="grid-gallery__image" src={images.url} alt={images.title}/>
            </div>
              
        ]
    })
    return to_return 
}
class Gallery extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            open:false,
            imageIndex:0
        }
        this.doClose = this.doClose.bind(this)
        this.doOpen = this.doOpen.bind(this)
    }
    doClose(){
        this.setState({
            open:false,
            imageIndex:0
        })
    }
    doOpen(index){
        this.setState({
            open:true,
            imageIndex:index
        })
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log(nextState);
        return nextState.open !== this.state.open ? true : false;
    }
    render(){
        console.log(this.props);
        return this.state.open === true 
            ? <Lightbox startIndex={this.state.imageIndex} images={this.props.imgs} onClose={this.doClose}></Lightbox>
            : <AllImages props={{onOpen:this.doOpen,images:this.props.imgs}}></AllImages>
                    
            
            
            
    }
}

export default Gallery