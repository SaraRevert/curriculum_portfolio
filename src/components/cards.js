import React from "react";
import {Link} from "gatsby"
import config from '../../config';
const make_card = (params)=>{
    return [
        <article key={params.title}>
            <main className="page-card-content">
                <div className="card" style={
                        {
                            backgroundImage:`url(${params.image})`,
                            backgroundSize: 'cover'
                        }
                    }>
                    <div className="card_content">
                        <h2 className="card_title">{params.title}</h2>
                        <p className="card_copy">{params.subtitle}</p>
                        <Link to={params.pageRoute}><button className="card_btn">View Trips</button></Link>
                    </div>
                </div>
            
            </main>
    </article>
    ]
}
export default function Cards(props) {
    let to_return = []
    let conditions = {"Front":5,"All":config.works.length}
    console.log(conditions[props.page]);
    
    to_return = config.works.slice(0,conditions[props.page]).map(work =>{
        const {title,subtitle,image,pageRoute} = work;
        return make_card({title:title,subtitle:subtitle,image:image,pageRoute:pageRoute})
    })
    
    return to_return;
    

}
