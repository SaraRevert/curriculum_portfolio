
import React from "react"
import config from '../../config';
function fromat_skill(params) {
    return [
        <section className="progress_container text-center" key={Math.random()}>
            <h3>{params.name}</h3>
            <div className="progress-bar ">
                <div className="progress" style={{
                    width: `${params.progress}%`,
                    backgroundColor: `#${params.color}`
                    
                    }}>
                    <div className="progress-shadow"></div>
                </div>
            </div>
        </section>
    ]
}
const ProgressBar = (props)=> {
    let to_return = []
    let conditions = {"Front":5,"All":config.skills.length}

        to_return = config.skills.slice(0, conditions[props.page]).map((item) => {
            const { name, progress ,color} = item;
            return fromat_skill({name:name,progress:progress,color:color})
        });

    return to_return
}

const CircularProgressBar = (props) =>{
    return [
        <div className="skills-container" key={Math.random()}>
            <div className="circular-progress html"></div>
            <div className="circular-progress css"></div>
            <div className="circular-progress javascript"></div>
            {/**<div className="circular-progress php"></div> */}
        </div>
    ]
}


export {
    ProgressBar,
    CircularProgressBar
}