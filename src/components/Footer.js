import React from 'react';
import config from '../../config';
export default function Footer() {
  return (
    <section id="footer">
      <div className="inner">
        <h2 className="major">Get in touch</h2>
        

        <div className="contact">

          {config.socialLinks.map(social => {
            const { icon, url, name ,color} = social;
            return (
                <a href={`${url}`} className={`social-card`} key={url}>
                  <article className="social_article"
                     style={{
                      color:color
                    }}
                  >
                  <span className={`social_span ${icon} icon`} ></span>
                  <h3>{name}</h3>
                  </article> 
                </a>
            );
          })}
        </div>
        <ul className="copyright">
          <li>
            Design: <a href="http://html5up.net">HTML5 UP</a>
          </li>
        </ul>
      </div>
    </section>
  );
}
