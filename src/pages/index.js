import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';
import Cards from "../components/cards"
import {ProgressBar} from "../components/cv-aptitude-bar"

import sara from '../assets/images/sara.png';


import config from '../../config';
const IndexPage = () => (
  <Layout fullMenu>
    
    <section id="banner">
      <div className="inner">
        <Link to="/"><h2>{config.heading}</h2></Link>
        <p>{config.subHeading}</p>
      </div>
    </section>
    <section id="wrapper">

      {/* Curriculum */}
     <section id="one" className="wrapper spotlight style1">
        <div className="inner">
          <div className="image">
            <img src={sara} alt="" />
          </div>
          <div className="content">
            <h2 className="major">Curriculum</h2>
            <p>
              Sara Revert Sanchis - 24 - Interiorismo 
            </p>
              <ProgressBar page="Front"></ProgressBar>
            <Link to="/curriculum" className="special">
              Ver Más
            </Link>
          </div>
        </div>
      </section>
      {/* Curriculum */}

      {/* ------------------------------- */}

      {/* Portfolio */}
     <section id="four" className="wrapper alt style1">
        <div className="inner">
          <h2 className="major">Portfolio</h2>
          {/* <p>
            
          </p> */}
          <section className="features">
              <Cards page="Front"></Cards>
          </section>
          <ul className="actions">
            <li>
            <Link to="/Portfolio" className="button">Browse All</Link>
            </li>
          </ul>
        </div>
      </section>
      {/* Portfolio */}
      
    </section>
  </Layout>
);

export default IndexPage;
