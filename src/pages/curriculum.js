import React from "react"
import Layout from '../components/Layout';
import {ProgressBar,CircularProgressBar} from "../components/cv-aptitude-bar"
import Timeline from "../components/timeline"
import config from '../../config';
import {Link} from "gatsby"

import sara from '../assets/images/sara.png';

const Curriculum = () => <Layout fullMenu>
    <section id="banner">
      <div className="inner">
        <Link to="/"><h2>{config.heading}</h2></Link>
        <p>{config.subHeading}</p>
      </div>
    </section>
    <section id="wrapper">
        <section id="one" className="wrapper spotlight style1"></section> {/* Relleno */}
        <section id="one" className="wrapper spotlight style1"> {/* Sobre Mi y mi formación*/}
            <div className="inner">
                <div className="image">
                        <img src={sara} alt="" />
                </div>
                <div className="content">
                    <h2 className="major">Sobre mí</h2>
                    <p> Sara Revert Sanchis - 24 - Interiorismo</p>
                    {/* Explicación Abaut Me */}
                    <div className="about_container">
                        <div className="about_info">
                            <p>Estudiante de interiorismo.</p>
                        </div>
                        <div className="about_contact">
                            <ul>
                                <li>Mail: sarainteriorssk@gmail.com</li>
                                <li>Gitlab: https://gitlab.com/SaraRevert/</li> 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className="sub-content">
                <div className="sub1">
                    <h2>Habilidades destacadas</h2>
                    <ProgressBar page="All"></ProgressBar>
                </div>
                <div className="sub2">
                    <h2>Work And Study TimeLine</h2>
                    <Timeline></Timeline>
                </div>
                <div className="sub3">
                    <h2>Otras Habilidades</h2>
                    <CircularProgressBar></CircularProgressBar>
                </div>
            </div>
        </section>
    </section>
</Layout>

export default Curriculum;
