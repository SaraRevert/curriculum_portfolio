import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/Crystal_Bath/Planimetria/Planta_distr.JPG',
		title: 'Planta de distribución'
  },
  {
		url: '/Crystal_Bath/Planimetria/Planta_font.JPG',
		title: 'Planta de fontanería'
  },
  {
		url: '/Crystal_Bath/Planimetria/Planta_sanej.JPG',
		title: 'Planta de saneamiento'
  },
  {
		url: '/Crystal_Bath/Planimetria/Planta_electr.JPG',
		title: 'Planta de electricidad e iluminación'
  },
  {
		url: '/Crystal_Bath/Planimetria/Sec_a.JPG',
		title: 'Sección A"-A'
  },
  {
		url: '/Crystal_Bath/Planimetria/Sec_b.JPG',
		title: 'Sección B-B"'
  },
  {
		url: '/Crystal_Bath/Planimetria/Sec_c.JPG',
		title: 'Sección C-C"'
  },
  {
		url: '/Crystal_Bath/Planimetria/Sec_d.JPG',
		title: 'Sección D-D"'
  },
  {
		url: '/Crystal_Bath/Planimetria/Alzado_ext.JPG',
		title: 'Alzados exteriores'
  },
  {
		url: '/Crystal_Bath/Planimetria/Detalls.JPG',
		title: 'Detalles constructivos'
	},
];
const renders = [
	{
		url: '/Crystal_Bath/Renders/Planta.JPG',
		title: 'Planta tridimensional'
  },
  {
		url: '/Crystal_Bath/Renders/Entrada.JPG',
		title: 'Zona de acceso'
  },
  {
		url: '/Crystal_Bath/Renders/Terma2.JPG',
		title: 'Zona de terma con cascada'
  },
  {
		url: '/Crystal_Bath/Renders/Cascada.JPG',
		title: 'Zona de terma con cascada'
  },
  {
		url: '/Crystal_Bath/Renders/Baño_banco.JPG',
		title: 'Zona de baño y asiento'
	},
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>Crystal Bath 2017</h2>
                  <p>Creación de un baño para el Concurso de Diseño de Baño CEVISAMA 2018</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de un baño para el Concurso de Diseño de Baño CEVISAMA 2018.
                          </p>
                          <p>
                          El baño está insprado en los antiguos baños japoneses, en los que la madera era el material predominante. Se combina con
                          su visión del agua como elemento relajante y con un toque moderno.
                          </p>
                          <p>
                          En el diseño de este baño predominan, madera (en pavimento, paredes y mobiliario) y cristal (en pavimento) , que,
                          nos proporciona una clara visión del agua, resultando un efecto relajante.
                          </p>
                          <p>
                          La estructura es un tanto peculiar, ya que existen varias alturas. Para acceder al baño, hay que subir unas escaleras
                          de madera, estas nos dan la altura suficiente para poder integrar en el espacio una segunda altura, la que pisamos,
                          que es cristal, y la inferior, no transitable. Una cascada hace fluir el agua, y que no quede estancada. Hay una zona
                          en la cual sí se puede hacer contacto con el agua de la cascada, habiendo habilitado en ella un banco de madera para
                          poder descansar y crear contraste con la transparencia del cristal. La zona donde se situa el baño y la bañera tiene
                          el suelo de madera, y paredes de pizarra, dándole así un toque natural al espacio.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
