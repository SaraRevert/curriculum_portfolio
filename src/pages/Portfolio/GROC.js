import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/GROC_2019/Planimetria/01.PNG',
		title: 'Planta módulo G cerrada'
  },
  {
		url: '/GROC_2019/Planimetria/02.PNG',
		title: 'Planta módulo G abierta'
  },
  {
		url: '/GROC_2019/Planimetria/03.PNG',
		title: 'Alzados módulo G izquierdo y frontal cerrados'
  },
  {
		url: '/GROC_2019/Planimetria/04.PNG',
		title: 'Alzados módulo G derecho y trasero cerrados'
  },
  {
		url: '/GROC_2019/Planimetria/05.PNG',
		title: 'Alzados módulo G izquierdo y frontal abiertos'
  },
  {
		url: '/GROC_2019/Planimetria/06.PNG',
		title: 'Secciones A y B módulo G'
  },
  {
		url: '/GROC_2019/Planimetria/07.PNG',
		title: 'Secciones C y D módulo G'
  },
  {
		url: '/GROC_2019/Planimetria/08.PNG',
		title: 'Planta electricidad módulo G'
  },
];
const renders = [
	{
		url: '/GROC_2019/Renders/Gext.jpg',
		title: 'Módulo G_Vista exterior'
  },
 	{
		url: '/GROC_2019/Renders/Gext2.jpg',
		title: 'Módulo G_Vista exterior'
  },
  {
		url: '/GROC_2019/Renders/Gprop.png',
		title: 'Módulo G_Vista detalle'
  },
  {
		url: '/GROC_2019/Renders/Gint.jpg',
		title: 'Módulo G_Vista interior'
  },
  {
		url: '/GROC_2019/Renders/Rext.jpg',
		title: 'Módulo R_Vista exterior'
  },
  {
		url: '/GROC_2019/Renders/Rint.jpg',
		title: 'Módulo R_Vista interior'
  },
  {
		url: '/GROC_2019/Renders/Oext.jpg',
		title: 'Módulo O_Vista exterior'
  },
  {
		url: '/GROC_2019/Renders/Cext.jpg',
		title: 'Módulo C_Vista exterior'
  },
  {
		url: '/GROC_2019/Renders/Cint.jpg',
		title: 'Módulo C_Vista interior'
  },
  {
		url: '/GROC_2019/Renders/Conjunta_RC.png',
		title: 'Módulos R y C_Vista conjunta'
  },
  {
		url: '/GROC_2019/Renders/moodboard2.png',
		title: 'Moodboard'
  },
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>GROC 2019</h2>
                  <p>Creación de puesto ambulante para la XXXI edición del concurso Premios habitácola 2019 de ARQUIN-FAD_Trabajo colaborativo con: Estela Semper
                  y Sandra Sánchez</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de puesto ambulante para la XXXI edición del concurso Premios habitácola 2019 de ARQUIN-FAD_Trabajo colaborativo con: Estela Semper y Sandra Sánchez.
                          </p>
                          <p>
                          El puesto ambulante planteado se compone de cuatro módulos creados a partir de cubos de 3x3x3m que se dividen en planos que pueden abrise, cerrarse y moverse a gusto del comerciante. Dichos módulos se han nombrado a partir del hilo principal del proyecto, el color amarillo, un color vibrante y llamativo. Encontramos el módulo G, diseñado por mí, el módulo R diseñado por Estela, el módulo O diseñado de forma conjunta y el C diseñado por Sandra. El conjunto formando GROC.
                          </p>
                          <p>
                          El módulo G se plantea como zona de ocio, el R como zona de venta de productos alimenticios, el O como zona de paso y descanso, y el módulo C como zona de exposiciones. Éstos módulos están planteados de forma que se puedan juntar creando distintas combinaciones entre ellos y que se puedan cerrar en un momento dado.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;

