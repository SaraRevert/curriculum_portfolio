import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/Oliplas/Planimetria/01_PlantaAct.JPG',
		title: 'Planta de estado actual'
  },
  {
		url: '/Oliplas/Planimetria/02_SecAct_AB.JPG',
		title: 'Sección de estado actual A-A" y B"-B'
  },
  {
    url: '/Oliplas/Planimetria/03_SecAct_CD.JPG',
		title: 'Sección de estado actual C-C" y D"-D'
  },
  {
    url: '/Oliplas/Planimetria/04_Asoleo.JPG',
		title: 'Planta de asoleo'
  },
  {
    url: '/Oliplas/Planimetria/05_PlantaDesc.JPG',
		title: 'Planta de distribución_Propuesta descartada'
  },
  {
    url: '/Oliplas/Planimetria/06_SecDesc_AB.JPG',
		title: 'Sección de propuesta descartada A-A" y B"-B'
  },
  {
    url: '/Oliplas/Planimetria/07_SecDescCD.JPG',
		title: 'Sección de propuesta descartada C-C" y D"-D'
  },
  {
    url: '/Oliplas/Planimetria/08_PlantaDef.JPG',
		title: 'Planta de distribución'
  },
];
const renders = [
	{
		url: '/Oliplas/Renders/00_Planta3D.JPG',
		title: 'Planta tridimensional'
  },
  {
		url: '/Oliplas/Renders/01_Mostrador.JPG',
		title: 'Zona de acceso y mostrador'
  },
  {
		url: '/Oliplas/Renders/02_Projector.JPG',
		title: 'Zona de proyección'
  },
  {
		url: '/Oliplas/Renders/03_Estant.JPG',
		title: 'Zona de exposición y stock'
  },
  {
		url: '/Oliplas/Renders/03_Estant2.JPG',
		title: 'Zona de exposición y stock'
  },
  {
		url: '/Oliplas/Renders/04_Bany.JPG',
		title: 'Baño'
  },
  {
		url: '/Oliplas/Renders/05_Provador2.JPG',
		title: 'Zona de provador cerrada'
  },
  {
		url: '/Oliplas/Renders/05_Provador1.JPG',
		title: 'Zona de provador abierta'
  },
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>OLIPLAS 2018</h2>
                  <p>Creación de una tienda llamada OLIPLAS (Océanos libres de plásticos) de ropa hecha con materiales reciclados</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          El proyecto OLIPLAS consta de una tienda de reducidas dimensiones donde se venderán piezas de ropa hechas con
                          materiales reciclados.
                          </p>
                          <p>
                          Se trata de una tienda ecoógica. Dicha tienda ha sido creada buscando contener el menor impacto posible en el
                          medio ambiente, y trasmitiendo el amor y cuidado a la naturaleza (más en concreto a los océanos), el ideal de
                          la marca se ve prsente en el local.
                          </p>
                          <p>
                          La tienda consta de líneas rectas que cruzan entre ellas. En el espacio encontramos muebles de cartón 100%
                          reciclables, o de maderas provenientes de bosques sostenibles, y se ha incoporado un inodoro con un mecanismo
                          que recicla el agua del lavamanos.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
