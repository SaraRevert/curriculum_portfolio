import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/Rest_Natsu/Planimetria/01_PlantaAct0.JPG',
		title: 'Planta de estado actual de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/02_PlantaAct1.JPG',
		title: 'Planta de estado actual de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/03_SecActA.JPG',
		title: 'Sección A-A" de estado actual'
  },
  {
		url: '/Rest_Natsu/Planimetria/04_SecActB.JPG',
		title: 'Sección B-B" de estado actual'
  },
  {
		url: '/Rest_Natsu/Planimetria/05_SecActC.JPG',
		title: 'Sección C-C" de estado actual'
  },
  {
		url: '/Rest_Natsu/Planimetria/06_SecActD.JPG',
		title: 'Sección D-D" de estado actual'
  },
  {
		url: '/Rest_Natsu/Planimetria/07_Planta0.JPG',
		title: 'Planta de distribución de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/08_Planta1.JPG',
		title: 'Planta de distribución de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/09_Asoleo.JPG',
		title: 'Planta de asoleamiento'
  },
  {
		url: '/Rest_Natsu/Planimetria/10_AlzadoFach.JPG',
		title: 'Alzado de fachada'
  },
  {
		url: '/Rest_Natsu/Planimetria/11_SecA.JPG',
		title: 'Sección A-A"'
  },
  {
		url: '/Rest_Natsu/Planimetria/12_SecB.JPG',
		title: 'Sección B-B"'
  },
  {
		url: '/Rest_Natsu/Planimetria/13_SecC.JPG',
		title: 'Sección C-C"'
  },
  {
		url: '/Rest_Natsu/Planimetria/14_SecD.JPG',
		title: 'Sección D-D"'
  },
  {
		url: '/Rest_Natsu/Planimetria/15_SecCoc.JPG',
		title: 'Sección de cocina'
  },
  {
		url: '/Rest_Natsu/Planimetria/16_Det1.JPG',
		title: 'Detalle constructivo de los paneles separadores'
  },
  {
		url: '/Rest_Natsu/Planimetria/16_Det2.JPG',
		title: 'Detalle constructivo de las tiras LED empotradas en la escalera'
  },
  {
		url: '/Rest_Natsu/Planimetria/16_Det3.JPG',
		title: 'Detalle constructivo de la estructura del falso techo de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/17_Font0.JPG',
		title: 'Planta de fontanería de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/17_Font1.JPG',
		title: 'Planta de fontanería de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/18_PlSan0.JPG',
		title: 'Planta de saneamiento de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/18_PlSan1.JPG',
		title: 'Planta de saneamiento de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/19_PlElectr0.JPG',
		title: 'Planta de electricidad e iluminación de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/19_PlElectr1.JPG',
		title: 'Planta de electricidad e iluminación de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/20_PlClim0.JPG',
		title: 'Planta de climatización de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/20_PlClim1.JPG',
		title: 'Planta de climatización de la planta -1'
  },
  {
		url: '/Rest_Natsu/Planimetria/21_PlInc0.JPG',
		title: 'Planta de protección contra incendios de la planta 0'
  },
  {
		url: '/Rest_Natsu/Planimetria/21_PlInc1.JPG',
		title: 'Planta de protección contra incendios de la planta -1'
  },
];
const renders = [
	{
		url: '/Rest_Natsu/Renders/0_PLanta0.JPG',
		title: 'Planta tridimensional de la planta 0'
  },
  {
		url: '/Rest_Natsu/Renders/0_Planta1.JPG',
		title: 'Planta tridimensional de la planta -1'
  },
  {
		url: '/Rest_Natsu/Renders/01_Barra0.JPG',
		title: 'Zona de comedor y barra de la planta 0'
  },
  {
		url: '/Rest_Natsu/Renders/02_KotatsuAb0.JPG',
		title: 'Zona de kotatsu de la planta 0 con paramento separador abierto'
  },
  {
		url: '/Rest_Natsu/Renders/02_KotatsuCr0.JPG',
		title: 'Zona de kotatsu de la planta 0 con paramento separador cerrado'
  },
  {
		url: '/Rest_Natsu/Renders/03_Escalera.JPG',
		title: 'Escalera, mitad superior'
  },
  {
		url: '/Rest_Natsu/Renders/05_EscaleraPers.JPG',
		title: 'Escalera, mitad inferior'
  },
  {
		url: '/Rest_Natsu/Renders/04_Comedor1.JPG',
		title: 'Zona de comedor de la planta -1'
  },
  {
		url: '/Rest_Natsu/Renders/06_KotatsuAb1.JPG',
		title: 'Zona de kotatsu de la planta -1 con paramento separador abierto'
  },
  {
		url: '/Rest_Natsu/Renders/06_KotatsuCr1.JPG',
		title: 'Zona de kotatsu de la planta -1 con paramento separador cerrado'
  },
  {
		url: '/Rest_Natsu/Renders/07_EscaleraAb1.JPG',
		title: 'Zona de comedor y escalera de la planta -1 con paramento separador lateral abierto'
  },
  {
		url: '/Rest_Natsu/Renders/07_EscaleraCr1.JPG',
		title: 'Zona de comedor y escalera de la planta -1 con paramento separador lateral cerado'
  },
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>Restaurante NatsuRamen 2020</h2>
                  <p>Creación de un restaurante japonés de dos plantas_Trabajo colaborativo con: Lidia Fernández</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de un restaurante de dos plantas. Nuestro cliente ficticio está basado en un
                          restaurante de ramen ya existente. Dicho cliente nos pediría crear un restaurante como expansión del actual. En él,
                          busca un ambiente japonés moderno, actual, y distinto al tradicional espacio que se puede encontrar en los locales
                          asiáticos que hay en la zona. Busca un ambiente elegante, discreto, sencillo y que sorprenda al espectador.
                          </p>
                          <p>
                          Para ello, hemos diseñado un restaurante basado en la cultura japonesa, integrando el uso de la madera como material
                          principal, la incorporación de jardines zen, unas zonas de kotatsu y además, la imitación del tatami, a otra escala
                          reconvertido en la estructura del falso techo de la planta -1. Todo ello dotando al espacio de un estilo japonés
                          modernizado y adaptado al local y las necesidades existentes. Se ha aprovechado también la iluminación natural del
                          espacio al máximo, y la iluminación artificial se ha basado en tiras LED y downlight que resaltan las geometrías del
                          espacio.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
