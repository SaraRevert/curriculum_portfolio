import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/HalfHouse/Planimetria/01_PlantaAct.JPG',
		title: 'Planta de estado actual'
  },
  {
		url: '/HalfHouse/Planimetria/02_PlaDistr.JPG',
		title: 'Planta de distribución'
  },
  {
		url: '/HalfHouse/Planimetria/03_PlaFont.JPG',
		title: 'Planta de fontanería'
  },
  {
		url: '/HalfHouse/Planimetria/04_PlaSan.JPG',
		title: 'Planta de saneamiento'
  },
  {
		url: '/HalfHouse/Planimetria/05_PlaElectr.JPG',
		title: 'Planta de electricidad e iluminación'
  },
  {
		url: '/HalfHouse/Planimetria/06_PlaClim.JPG',
		title: 'Planta de Climatización'
  },
];
const renders = [
	{
		url: '/HalfHouse/Renders/07_Planta3D.JPG',
		title: 'Planta tridimensional'
  },
  {
		url: '/HalfHouse/Renders/08_Entrada.JPG',
		title: 'Zona de acceso recepción'
  },
  {
		url: '/HalfHouse/Renders/09_Cuina.JPG',
		title: 'Zona de cocina-comedor'
  },
  {
		url: '/HalfHouse/Renders/09_Cuina2.JPG',
		title: 'Zona de cocina-comedor'
  },
  {
		url: '/HalfHouse/Renders/10_Treball.JPG',
		title: 'Zona de taller y almacenaje'
  },
  {
		url: '/HalfHouse/Renders/11_Exterior.JPG',
		title: 'Terraza'
  },
  {
		url: '/HalfHouse/Renders/11_Exterior2.JPG',
		title: 'Terraza'
  },
  {
		url: '/HalfHouse/Renders/12-Bany.JPG',
		title: 'Baño'
  },
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>HalfHouse 2017</h2>
                  <p>Creación de una vivienda apta para un artista que residirá de forma temporal</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de una vivienda apta para un artista que permanecerá por un periodo de tiempo
                          aproximado a un mes residiendo y trabajando en sus obras. Al final de este lapso de tiempo acordado con el artista,
                          esta vivienda será abierta al público para la exposición del trabajo de dicho artista en ese tiempo. Esto aporta al
                          artista un cambio de hábitat con nuevos puntos de vista e inspiraciones, y a las personas, residentes en la zona
                          donde se encuentra la vivienda, les proporciona otros puntos de vista culturales y artísticos.
                          </p>
                          <p>
                          La vivienda consta de formas principalmente curvas que aportan al espacio dinamismo. Éste espacio está pensado para
                          una sola persona que pueda trabajar y a su vez vivir en él, por lo que cuenta con dormitorio, baño privado,
                          cocina-comedor, salón, taller, balcón y jardín. Es un espacio diáfano en la zona pública ya que el artista elegirá
                          en qué zona exponer su/sus obras, ya sea en el interior o al aire libre.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
