import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/Subiba/Planimetria/01_PlantaAct2.JPG',
		title: 'Planta en estado actual'
  },
  {
		url: '/Subiba/Planimetria/03_SecAct2.JPG',
		title: 'Secciones en estado actual A"-A y B"-B'
  },
  {
		url: '/Subiba/Planimetria/04_PlantaDistr2.JPG',
		title: 'Planta de distribución'
  },
  {
		url: '/Subiba/Planimetria/04_PlantaZoniEmpr.JPG',
		title: 'Planta zonificada por empresas'
  },
  {
		url: '/Subiba/Planimetria/08_PlantaIluTecho2.JPG',
		title: 'Planta de iluminación de techo'
  },
  {
		url: '/Subiba/Planimetria/09_PlantaIluSuelo2.JPG',
		title: 'Planta de iluminación de suelo'
  },
  {
		url: '/Subiba/Planimetria/10_SecLongABC2.JPG',
		title: 'Secciones longitudinales A-A", B-B" y C-C"'
  },
  {
		url: '/Subiba/Planimetria/11_SecTransvDEF2.JPG',
		title: 'Secciones transversales D-D", E-E" y F-F"'
  },
  {
		url: '/Subiba/Planimetria/12_PlantaText2.JPG',
		title: 'Planta de distribución texturizada'
  },
  {
		url: '/Subiba/Planimetria/13_SecLongEText2.JPG',
		title: 'Sección transversal E-E" texturizada'
  },
];
const renders = [
	{
		url: '/Subiba/Renders/1_Recepcion.JPG',
		title: 'Zona de recepción'
  },
  {
		url: '/Subiba/Renders/2_Comedor.JPG',
		title: 'Zona de Azulejos porcelanosa_Comedor'
  },
  {
		url: '/Subiba/Renders/3_Patio.JPG',
		title: 'Zona de Venis_Patio'
  },
  {
		url: '/Subiba/Renders/4_Cocina.JPG',
		title: 'Zona de Gamadecor_Cocina'
  },
  {
		url: '/Subiba/Renders/5_Dormitorio.JPG',
		title: 'Zona de L`Antic Colonial_Dormitorio'
  },
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>Subiba 2018</h2>
                  <p>Creación de un showroom para la XI edición del concurso Premios arquitectura e interiorismo 2018 de PORCELANOSA Grupo_Trabajo colaborativo con: Estela Semper
                  y Sandra Sánchez</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de un showroom para la XI edición del concurso Premios arquitectura e interiorismo
                          2018 de PORCELANOSA Grupo. Trabajo colaborativo con: Estela Semper y Sandra Sánchez.
                          </p>
                          <p>
                          El showroom planteado es un espacio fluido, que guía al visitante a través de las entidades de la empresa PORCELANOSA
                          Grupo, ordenadas cronológicamente. El espacio está ambientado en una vivienda, dentro de la cual, cada firma se manifiesta
                          en la estancia más adecuada a sus características.
                          </p>
                          <p>
                          Para ello, se plantea mantener el estilo expositivo que sigue la marca PORCELANOSA Grupo, ésto es, una manera sobria y
                          elegante. Como contrapunto se ha introducido un elemento más vivo mediante una estructura de krion, que se convierte en
                          el hilo conductor del recorrido. Esta estructura, modelada por nuestro equipo, le da vida al espacio por su dinamismo y
                          versatilidad, formando suelos, pasarelas, bancos, mesas y techo. Otro elemento de contraste respecto al estilo de
                          PORCELANOSA Grupo, es la incorporación de elementos vegetales, con la intención de introducir un elemento natural en el
                          showroom.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
