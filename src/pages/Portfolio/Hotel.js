import React from 'react';
import Layout from '../../components/Layout';
import Gallery from '../../components/gallery';

const perimetria = [
	{
		url: '/Hotel_2017/Perimetria/Planta original foto.PNG',
		title: 'Planta original'
	},
	{
		url: '/Hotel_2017/Perimetria/Planta final vacia foto.PNG',
		title: 'Planta vacía'
	},
	{
		url: '/Hotel_2017/Perimetria/Planta amueblada foto.PNG',
		title: 'Planta de distribución'
	},
	{
    url: '/Hotel_2017/Perimetria/0_Planta_textura.JPG',
		title: 'Planta de distribución texturizada'
	},
	{
		url: '/Hotel_2017/Perimetria/AlçatA.JPG',
		title: 'Alzado A"-A'
  },
  {
		url: '/Hotel_2017/Perimetria/AlçatB.JPG',
		title: 'Alzado B"-B'
	},
	{
    url: '/Hotel_2017/Perimetria/AlçatC.JPG',
		title: 'Alzado C"-C'
  },
  {
    url: '/Hotel_2017/Perimetria/AlçatD.JPG',
		title: 'Alzado D"-D'
  },
];
const renders = [
	{
		url: '/Hotel_2017/Renders/Planta.JPG',
		title: 'Planta tridimensional'
	},
	{
		url: '/Hotel_2017/Renders/2_Escriptori.JPG',
		title: 'Zona de trabajo'
  },
  {
		url: '/Hotel_2017/Renders/3_Social2.JPG',
		title: 'Zona de ocio'
	},
	{
		url: '/Hotel_2017/Renders/4_Social.JPG',
		title: 'Zona de ocio'
	},
	{
    url: '/Hotel_2017/Renders/5_Vestidor.JPG',
		title: 'Vestidor'
	},
	{
		url: '/Hotel_2017/Renders/6_Vestidor.JPG',
		title: 'Vestidor'
	},
	{
    url: '/Hotel_2017/Renders/7_Bany.JPG',
		title: 'Baño'
  },
  {
    url: '/Hotel_2017/Renders/8_Llit.JPG',
		title: 'Zona de descanso'
  },
  {
    url: '/Hotel_2017/Renders/9_Llit.JPG',
		title: 'Zona de descanso'
  },
  {
    url: '/Hotel_2017/Renders/10_Vista.JPG',
		title: 'Vista general'
	},
];



class Hotel extends React.Component {

  render() {
    return(
      <Layout fullMenu>
        <section id="wrapper">
            <section id="banner">
                <div className="inner">
                  <h2>Hotel 2017</h2>
                  <p>Creación de una habitación de hotel para la segunda edición del concurso InterCIDEC</p>
                </div>
            </section>
              <section id="four" className="wrapper alt style1">
                <div className="inner">
                  <div className="with-gallery" >
                    <div className="sub1" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva galeria */}
                        <h2>Renders</h2>
                        <div className="photo-grid">
                          <Gallery imgs={renders}></Gallery>
                        </div>
                        <h2>Planimetría</h2>
                        <div className="photo-grid">
                          <Gallery imgs={perimetria}></Gallery>
                        </div>
                      {/* Contenido a borrar en nueva galeria */}
                    </div>
                    <div className="sub3" style={{width:"50%"}}>
                      {/* Contenido a borrar en nueva descripicion */}
                        <h2>Información del proyecto</h2>
                          <p>
                          Este proyecto consiste en la creación de una habitación de hotel para la segunda edición 
                          del concurso InterCIDEC, celebrado en 2017, organizado por Beltá & Frajumar. Para ello se proporcionó la 
                          planta vacía de una habitación de hotel.
                          </p>
                          <p>
                          El diseño de la habitación resultante se enfoca en transmitir transparencia y reflexión mediante espejos,
                          éste concepto procede del principal referente, "The Mirror Cube". Éste referente es una habitación perteneciente
                          al hotel "Tree Hotel", la habitación es un cubo de espejos que se mimetiza con el entorno, dejando una ilusión
                          de transparencia en el bosque.
                          </p>

                          <p>La distribución resultante del diseño es de apariencia diáfana, que se complementa con el concepto de la 
                          transparencia mediante diversos espejos dispuestos por la habitación.
                          La habitación cuenta con un vestidor, un baño de color blanco transmitiendo sensación de pureza y luminosidad. 
                          La estancia principal se conforma de diversas zonas cohesionadas mediante el uso de los espejos y del color 
                          azul, que nos recuerda al cielo azul.</p>
                          <p>Nada más entrar se puede observar la habitación al completo, donde destaca una cama estilo "bed up" con dos mesillas 
                          de noche i una televisión colgada, esta zona, se consideraría el espacio de descanso. Junto a ella se observa un banco de 
                          ventana, una silla colgante y más a la derecha un sofá, todo ello forma la zona social de la estancia. Por último, junto al 
                          acceso se encuentra un escritorio con una mini nevera integrada en dicha mesa, esto se consideraría la zona de trabajo y 
                          mini bar.</p>
                          <br></br>
                          <p>
                          Los materiales y texturas que destacan en la estancia son el micro-cemento pulido presente en todo el suelo, combinado con 
                          dos franjas de césped que dan un toque natural y acogedor a la neutralidad del micro-cemento. A ello se le suman las transparencias 
                          y reflexiones del metacrilato y vidrios azules que nos recuerdan al cielo y los espejos que nos unen al concepto de transparencia.
                          </p>

                      {/* Contenido a borrar en nueva descripcion */}
                    </div>
                  </div>
                </div>
              </section>
        </section>
      </Layout>
    )
  }
}



export default Hotel;
