import React from 'react';

import Layout from '../components/Layout';
import Cards from "../components/cards"

const IndexPage = () => (
  <Layout fullMenu>
    <section id="wrapper">
      <section id="banner">
        <div className="inner">
          <h2>Portfolio</h2>
        </div>
      </section>

      <div className="wrapper">
        <div className="inner">
          <section className="features">    
              <Cards page="All"></Cards>
          </section>
        </div>
      </div>
    </section>
  </Layout>
);

export default IndexPage;
