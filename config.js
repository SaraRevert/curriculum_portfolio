module.exports = {
  siteTitle: 'Curriculum Sara Revert', // <title>
  manifestName: 'Viaje antes que destino',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/website-icon.png',
  pathPrefix: `/gatsby-starter-solidstate/`, // This path is subpath of your hosting https://domain/portfolio
  heading: ' Portfolio Sara ',
  //subHeading: 'Vida antes que Muerte. Viaje antes que destino. Fuerza antes que debilidad. ',
  // social
  socialLinks: [
    {
      icon: 'fab fa-instagram',
      name: 'Instagram',
      color:"#EA4C89",
      url: 'https://www.instagram.com/sarainteriorss/',
    },
  ],
  //Curriculum: Habilidades
  skills:[
    {name:"AutoCad",progress:"80",color:"5d6a8b"},
    {name:"SketchUp",progress:"80",color:"5d6a8b"},
    {name:"V-ray",progress:"65",color:"5d6a8b"},
    {name:"Arquímedes",progress:"60",color:"5d6a8b"},
    {name:"InDesign",progress:"70",color:"5d6a8b"},
    {name:"Illustrator",progress:"70",color:"5d6a8b"},
    {name:"Photoshop",progress:"60",color:"5d6a8b"},
    {name:"3dsMax",progress:"40",color:"5d6a8b"},
    {name:"ZBrush",progress:"40",color:"5d6a8b"},
    {name:"Meshmixer",progress:"20",color:"5d6a8b"},
    {name:"Aspire",progress:"20",color:"5d6a8b"},
  ],
  //Trabajos del portfolio: Los cuatro primeros se veran en la pagina principal
  /**
   * {title:"Titulocarta",subtitle:"explicacion de lo que es",
     image:"../../Carpeta de static a crear/nombre_imagen.extension",pageRoute:"../Portfolio/NombreFicheroSinExtension"},
   */
  works:[
    {title:"Restaurante NatsuRamen 2020",subtitle:"Creación de un restaurante japonés de dos plantas_Trabajo colaborativo con: Lidia Fernández",
    image:"../../Rest_Natsu/07_EscaleraCr1.JPG",pageRoute:"../Portfolio/Rest_Natsu"},
    {title:"GROC 2019",subtitle:"Creación de puesto ambulante para la XXXI edición del concurso Premios habitácola 2019 de ARQUIN-FAD_Trabajo colaborativo con: Estela Semper y Sandra Sánchez",
    image:"../../GROC_2019/G_ext2copia.jpg",pageRoute:"../Portfolio/GROC"},
    {title:"Crystal Bath 2017",subtitle:"Creación de un baño para el Concurso de Diseño de Baño CEVISAMA 2018",
    image:"../../Crystal_Bath/Cascada.JPG",pageRoute:"../Portfolio/CrystalBath"},
    {title:"Subiba 2018",subtitle:"Creación de un showroom para la XI edición del concurso Premios arquitectura e interiorismo 2018 de PORCELANOSA Grupo_Trabajo colaborativo con: Estela Semper y Sandra Sánchez",
    image:"../../Subiba/2_Comedor.JPG",pageRoute:"../Portfolio/Subiba"},
    {title:"OLIPLAS 2018",subtitle:"Creación de una tienda llamada OLIPLAS (Océanos libres de plásticos) de ropa hecha con materiales reciclados",
    image:"../../Oliplas/03_Estant2.JPG",pageRoute:"../Portfolio/Oliplas"},
    {title:"HalfHouse 2017",subtitle:"Creación de una vivienda apta para un artista que residirá de forma temporal",
    image:"../../HalfHouse/09_Cuina.JPG",pageRoute:"../Portfolio/HalfHouse"},
    {title:"Hotel 2017",subtitle:"Creación de una habitación de hotel para la segunda edición del concurso InterCIDEC",
     image:"../../Hotel_2017/Vista_General.JPG",pageRoute:"../Portfolio/Hotel"},
  ],
  time_line:[
    {tittle:"IES L'Estació - Ontinyent",time:"2014-2016",desc:"Bachillerato Artístico"},
    {tittle:"The Cambridge School - Alcoy",time:"2018-2019",desc:"Inglés B2"},
    {tittle:"Escola d'animació d'Acció Cultural del País Valencià - Ontinyent",time:"2020",desc:"Curso de Monitor de Actividades de Tiempo Libre educativo infantil y juvenil"},
    {tittle:"Matter - Barcelona",time:"16 marzo 2021-29 abril 2021",desc:"Prácticas de empresa 240h"},
    {tittle:"EASD - Alcoy",time:"2016-2023",desc:"Grado en enseñanzas artísticas superiores de Diseño de Interiores"},

  ]
  
};
